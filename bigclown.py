#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from influxdb import InfluxDBClient
import time
import os
import configparser
import cznicdevices
import random
import sys
import datetime

# Thresholds #

TEMP_IN_LOW = 20.0
TEMP_IN_HIGH = 27.0

TEMP_OUT_LOW = 0.0
TEMP_OUT_HIGH = 27.0

HUM_IN_LOW = 20.0
HUM_IN_HIGH = 80.0

HUM_OUT_LOW = 40.0
HUM_OUT_HIGH = 101.0

CO2_LOW = -1.0
# CO2_WARN = 1000.0
CO2_HIGH = 1200.0

AIR_OK = 1
AIR_LOW = -1
AIR_WARN = 2
AIR_HIGH = 3

PM25_WARN = 50
PM25_HIGH = 100

PM10_WARN = 40
PM10_HIGH = 90

SO2_WARN = 50
SO2_HIGH = 350

O3_WARN = 65
O3_HIGH = 180

NO2_WARN = 50
NO2_HIGH = 200

MAX_ALERTS = 100
MAX_LATEST_ALERT = 3600

ALERT_FROM = 8  # including
ALERT_TO = 18  # not including

NOALERT_WEEKDAYS = [5, 6]

VALUE_LOW = -1
VALUE_OK = 0
VALUE_HIGH = 1

ALERT_INSIDE = True
ALERT_TEMP_IN = True
AlERT_HUM_IN = False
ALERT_CO2 = True

ALERT_OUTSIDE = False
ALERT_TEMP_OUT = False
ALERT_HUM_OUT = False
ALERT_AIR = True

INFO_INSIDE = True
INFO_TEMP_IN = True
INFO_HUM_IN = True
INFO_CO2 = True

INFO_OUTSIDE = True
INFO_TEMP_OUT = True
INFO_HUM_OUT = True
INFO_AIR = True


class BigClownAlerter:
    def __init__(self, influxconfigfile=None, deviceconfigfile=None):
        self.verbose = True
        self.trigger_always = False

        self.influxconfigfile = influxconfigfile
        self.deviceconfigfile = deviceconfigfile

        self.load_influxconfigfile()
        self.load_deviceconfigfile()

        self.notifications = {}
        self.last_notifications = {}

        self.last_data = {}
        # self.trigger_always = True  # TEMPORARY DEBBUGGING

    def debug(self, *args, **kwargs):
        if self.verbose:
            print(*args, file=sys.stderr, **kwargs)
            sys.stderr.flush()

    def print(self, *args, **kwargs):
        print(*args, file=sys.stdout, **kwargs)

    def load_influxconfigfile(self):
        if self.influxconfigfile is None:
            self.influxconfigfile = os.path.join(os.path.dirname(
                                    os.path.realpath(__file__)), "influx.ini")
        self.influxconfig = configparser.ConfigParser({
            "host": "localhost",
            "port": "8086",
            "db": "devtest",
            "user": "user",
            "pass": "pass",
            })
        self.influxconfig.read(self.influxconfigfile)

        self.influx_host = self.influxconfig.get('influx', 'host')
        self.influx_port = int(self.influxconfig.get('influx', 'port'))
        self.influx_db = self.influxconfig.get('influx', 'db')
        self.influx_user = self.influxconfig.get('influx', 'user')
        self.influx_password = self.influxconfig.get('influx', 'pass')

        if self.influx_db == "devtest":
            print("WARNING!!! DEBUG MODE!!!")
            self.verbose = True
            self.trigger_always = True

        self.debug("influx host:", self.influx_host)
        self.debug("influx port:", self.influx_port)
        self.debug("influx db:", self.influx_db)
        self.debug("influx user:", self.influx_user)

        # connect to influx
        if self.influx_db != "devtest":
            self.influx = InfluxDBClient(self.influx_host,
                                         self.influx_port,
                                         self.influx_user,
                                         self.influx_password,
                                         self.influx_db)

    def load_deviceconfigfile(self):
        if self.deviceconfigfile is None:
            self.deviceconfigfile = os.path.join(os.path.dirname(
                                    os.path.realpath(__file__)), "devices.yml")
        #self.devices = cznicdevices.get_devicesid("devices.yml")
        self.devices = cznicdevices.get_devicesid(
                os.path.join(os.path.dirname(
                    os.path.realpath(__file__)), "devices.yml"))
        self.debug("Found", len(self.devices), "devices")

    def run_bot(self):
        pass

    def run_alert_once(self):
        for deviceid, device in self.devices.items():
            # if self.verbose:
                # self.debug("-", deviceid, end='')
                # if "name" in device:
                    # print("", device["name"], end='')
                # if "loc" in device:
                    # print("", device["loc"], end='')
                # print()
            self.get_data_for_room(device)
        pass

    def run_alert(self, timeout=600):
        while True:
            self.run_alert_once()
            self.debug("---- sleeeping for "+str(timeout)+" seconds ----")
            time.sleep(timeout)

    def get_air_index(self, pm25, pm10, so2, o3, no2):
        index = AIR_OK
        # PM25
        if pm25 is not None:
            if pm25 >= PM25_HIGH:
                return AIR_HIGH
            if pm25 >= PM25_WARN and index is AIR_OK:
                index = AIR_WARN
        # PM10
        if pm10 is not None:
            if pm10 >= PM10_HIGH:
                return AIR_HIGH
            if pm10 >= PM10_WARN and index is AIR_OK:
                index = AIR_WARN
        # SO2
        if so2 is not None:
            if so2 >= SO2_HIGH:
                return AIR_HIGH
            if so2 >= SO2_WARN and index is AIR_OK:
                index = AIR_WARN
        # O3
        if o3 is not None:
            if o3 >= O3_HIGH:
                return AIR_HIGH
            if o3 >= O3_WARN and index is AIR_OK:
                index = AIR_WARN
        # NO2
        if no2 is not None:
            if no2 >= NO2_HIGH:
                return AIR_HIGH
            if no2 >= NO2_WARN and index is AIR_OK:
                index = AIR_WARN
        return index

    def validate_two(self, values, low, high):
        if values is not None and len(values) == 2:
            # prilis nizka a nebylo
            if values[0] <= low and values[1] > low:
                return VALUE_LOW
            # prilis vysoka a nebylo
            if values[0] >= high and values[1] < high:
                return VALUE_HIGH
            # v poradku
            if values[0] > low and values[0] < high:
                # ale nebylo
                if values[1] <= low or values[1] >= high:
                    return VALUE_OK
        return None

    def validate_one(self, value, low, high):
        if value is not None:
            # prilis nizka
            if value <= low:
                return VALUE_LOW
            # prilis vysoka
            if value >= high:
                return VALUE_HIGH
            # v poradku
            if value > low and value < high:
                    return VALUE_OK
        return None

    def evaluation_state(self, device, ti, to, hi, ho, co2, air):
        room = device["id"]
        if "loc" in device:
            room = device["loc"]
        elif "name" in device:
            room = device["name"]

        if ti is not None or hi is not None or co2 is not None:
            self.debug("-", room, "ti:", ti, "to:", to, "hi:", hi, "ho:", ho, "co2:", co2, "air:", air)

        ti_state = self.validate_two(ti, TEMP_IN_LOW, TEMP_IN_HIGH)
        hi_state = self.validate_two(hi, HUM_IN_LOW, HUM_IN_HIGH)
        co2_state = self.validate_two(co2, CO2_LOW, CO2_HIGH)
        to_state = self.validate_one(to, TEMP_OUT_LOW, TEMP_OUT_HIGH)
        ho_state = self.validate_one(ho, HUM_OUT_LOW, HUM_OUT_HIGH)
        air_state = self.validate_one(air, AIR_LOW, AIR_HIGH)

        message = ""

        if ti_state is not None:
            if ti_state == VALUE_HIGH:
                message += "Teplota v mistnosti "+room+" je prilis vysoka ("+str(ti[0])+" C).\n"
            if ti_state == VALUE_LOW:
                message += "Teplota v mistnosti "+room+" je prilis nizka ("+str(ti[0])+" C).\n"
            if ti_state == VALUE_OK:
                message += "Teplota v mistnosti "+room+" zacina byt v poradku ("+str(ti[0])+" C).\n"

        if hi_state is not None:
            if hi_state == VALUE_HIGH:
                message += "Vlhkost v mistnosti "+room+" je prilis vysoka ("+str(hi[0])+" %).\n"
            if hi_state == VALUE_LOW:
                message += "Vlhkost v mistnosti "+room+" je prilis nizka ("+str(hi[0])+" %).\n"
            if hi_state == VALUE_OK:
                message += "Vlhkost v mistnosti "+room+" zacina byt v poradku ("+str(hi[0])+" %).\n"

        if co2_state is not None:
            if co2_state == VALUE_HIGH:
                message += "Koncetrace co2 v mistnosti "+room+" je prilis vysoka ("+str(co2[0])+" ppm).\n"
            if co2_state == VALUE_LOW:
                message += "Koncetrace co2 v mistnosti "+room+" je prilis nizka ("+str(co2[0])+" ppm).\n"
            if co2_state == VALUE_OK:
                message += "Koncetrance co2 v mistnosti "+room+" zacina byt v poradku ("+str(co2[0])+" ppm).\n"

        if message != "":
            # Pridame informace o pocasi venku.
            if to_state is not None:
                if to_state == VALUE_HIGH:
                    message += "Venku je horko ("+str(to)+" C). "
                if to_state == VALUE_LOW:
                    message += "Venku je zima ("+str(to)+" C). "
                if to_state == VALUE_OK:
                    message += "Venku je "+str(to)+" C. "

            if ho_state is not None:
                if ho_state == VALUE_HIGH:
                    message += "Venku je prilis velka vlhkost ("+str(ho)+" %). "
                if ho_state == VALUE_LOW:
                    message += "Venku je prilis nizka vlhkost ("+str(ho)+" %). "
                if ho_state == VALUE_OK:
                    message += "Venku je "+str(ho)+" % vlhkosti. "

            if air_state is not None:
                if air_state == VALUE_HIGH:
                    message += "Venku je spatne ovzdusi ("+str(air)+" stupen). "
                if air_state == VALUE_LOW:
                    message += "Venku je spatne ovzdusi ("+str(air)+" stupen). "
                if air_state == VALUE_OK:
                    message += "Ovzdusi venku je v poradku"

        return message

    def evaluation(self, device,
                   ti, to, hi, ho, co2, pm25, pm10, so2, o3, no2):
        air = self.get_air_index(pm25, pm10, so2, o3, no2)

        message = self.evaluation_state(device, ti, to, hi, ho, co2, air)
        if message is not None and message != "":
            self.print("-", message)
            self.send_alert_for_room(device, message)

    def get_last(self, meassurement, where, limit=1):
        query = "SELECT * FROM \""+meassurement+"\" WHERE "+where+" ORDER BY DESC LIMIT "+str(limit)
        # if self.verbose:
        #     print(query)
        if self.influx_db == 'devtest':
            if meassurement == "concentration":
                return [round(random.uniform(500.0, 2000.0), 2), round(random.uniform(500.0, 2000.0), 2)]
            if meassurement == "temperature":
                return [round(random.uniform(-10, 40), 2), round(random.uniform(-10, 40), 2)]
            if meassurement == "relative-humidity":
                return [round(random.uniform(10, 90), 2), round(random.uniform(10, 90), 2)]
            if meassurement == "ext_temp":
                return round(random.uniform(-10, 40), 2)
            if meassurement == "ext_humidity":
                return round(random.uniform(0, 100), 2)
            return None
        resultset = self.influx.query(query)
        # print(resultset)
        resultlist = list(resultset.get_points())
        # print(resultlist)
        if len(resultlist) == 0:
            self.debug("warning, zero length of resullist!", meassurement, where)
            return None
        if limit == 1:
            result = float(resultlist[0]["value"])
        else:
            result = []
            for item in resultlist:
                result.append(float(item["value"]))
        # self.debug("get_last", meassurement, result)
        return result

    def save_last(self, deviceid, meassurement, current):
        if deviceid not in self.last_data:
            self.last_data[deviceid] = {}
        # if meassurement not in self.last_data[deviceid]:
            # self.last_data[deviceid][meassurement] = []
        self.last_data[deviceid][meassurement] = current

    def last_is_same(self, deviceid, meassurement, current):
        is_same = True
        if current is None:
            return True
        if deviceid not in self.last_data or meassurement not in self.last_data[deviceid]:
            # no last data
            self.debug("INFO: No last data", meassurement, "-> not same", current)
            is_same = False
        else:
            # last_device = self.last_data[deviceid]
            last_data = self.last_data[deviceid][meassurement]
            # self.debug("last and current data:", meassurement, last_data, current)
            if len(last_data) != len(current) and len(last_data) != 2:
                # self.debug("INFO: Not the same count of data", meassurement, "-> not same")
                # wrong last or current data -> no alert
                is_same = True
            if last_data[0] != current[0] or last_data[1] != current[1]:
                # self.debug("INFO: NOT the same as last", meassurement, "-> skipping", last_data, current)
                is_same = False
        self.save_last(deviceid, meassurement, current)
        # if is_same:
            # self.debug("INFO: the same as last", meassurement, "-> skipping", last_data, current)
        return is_same

    def get_data_for_room(self, device):
        if "loc" not in device:
            return
        # openweathermap
        if "openweathermap" in device:
            openweathermap = "location='"+device["openweathermap"]+"'"
            to = self.get_last("ext_temp", openweathermap)
            ho = self.get_last("ext_humidity", openweathermap)
        else:
            to = None
            ho = None
        # aqicn
        if "aqicn" in device:
            aqicn = "location='"+device["aqicn"]+"'"
            pm25 = self.get_last("ext_pm25", aqicn)
            pm10 = self.get_last("ext_pm10", aqicn)
            so2 = self.get_last("ext_so2", aqicn)
            o3 = self.get_last("ext_o3", aqicn)
            no2 = self.get_last("ext_no2", aqicn)
        else:
            pm25 = None
            pm10 = None
            so2 = None
            o3 = None
            no2 = None
        # bigclown device
        deviceid = "device_id='"+str(device["id"])+"'"
        tis = self.get_last("temperature", deviceid, 2)
        if self.last_is_same(device["id"], "temperature", tis):
            tis = None
        his = self.get_last("relative-humidity", deviceid, 2)
        if self.last_is_same(device["id"], "relative-humidity", his):
            his = None
        co2s = self.get_last("concentration", deviceid, 2)
        if self.last_is_same(device["id"], "concentration", co2s):
            co2s = None
        self.evaluation(device, tis, to, his, ho, co2s, pm25, pm10, so2, o3, no2)

    def is_too_many_alerts(self, username):
        if username not in self.notifications:
            self.notifications[username] = 0
        if username not in self.last_notifications:
            self.last_notifications[username] = 0

        if (time.time() - self.last_notifications[username]) <= MAX_LATEST_ALERT:  # Notifikace nedavno
            if self.notifications[username] >= MAX_ALERTS:  # a prilis krat
                self.debug("skipping alerting because of it's too frequent!")
                return True
        return False

    def count_alert(self, username):
        self.notifications[username] += 1
        self.last_notifications[username] = time.time()

    def send_alert_to_jabber(self, jabber, message):
        self.debug("!!! alerting to ", jabber, message.encode('utf-8'))
        os.system("./xsend.py "+jabber+" \""+message+"\" 1>&2")

    def send_alert_to_telegram(self, telegram, message):
        self.debug("!!! alerting to ", telegram, message.encode('utf-8'))
        os.system("telegram-send --config "+telegram+" \""+message+"\" 1>&2")

    def send_alert_to_mattermost(self, mattermost, message):
        self.debug("!!! alerting to ", mattermost, message.encode('utf-8'))
        os.system("echo \""+message+"\" | mattersend -U "+mattermost+" 1>&2")

    def send_alert_for_room(self, device, message):
        if message is None or message == "":
            return
        if self.influx_db == "devtest":
            message = "TESTING: "+message
        if "notifications" in device:
            for username, notification in device["notifications"].items():
                now = datetime.datetime.now()
                if now.weekday() in NOALERT_WEEKDAYS:
                    self.debug("Not alert day!", now.weekday(), NOALERT_WEEKDAYS)
                    return
                alert_from = now.replace(hour=ALERT_FROM, minute=0, second=0, microsecond=0)
                alert_to = now.replace(hour=ALERT_TO, minute=0, second=0, microsecond=0)
                if now < alert_from or now > alert_to:
                    self.debug("Not alert time!", alert_from, "<", now, "<", alert_to)
                    return
                if self.is_too_many_alerts(username):
                    return
                self.count_alert(username)
                self.debug("notification for", username)
                if "jabber" in notification:
                    self.send_alert_to_jabber(notification["jabber"], message)
                if "telegram" in notification:
                    self.send_alert_to_telegram(notification["telegram"],
                                                message)
                if "mattermost" in notification:
                    self.send_alert_to_mattermost(notification["mattermost"],
                                                  message)


if __name__ == '__main__':
    alerter = BigClownAlerter()
    # default mode is not xmpp bot
    alerter.run_alert(10)
