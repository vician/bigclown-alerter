# Notifikace

- Vyvetrat (prilis velka koncentrace co2)
- Vyvetrat (prilis nizka vlkhost a venku je vetsi)
- Zacit klimatizovat (prilis velka teplota proti venku)
- Prestat klimatizovat (venku je chladneji nez vevnitr)
	- jak detekovat ze se klimatizuje
- Zapnout/pridat topeni
- Vypnout/ubrat topeni
- Rozsvitit
- Zhasnout
- Nevetrat, spatna kvalita ovzdusi

# Stavy

- ti: Vnitrni teplota: nizka, normalni, vysoka (20,27)
- to: Venkovni teplota: nizka, normalni, vysoka (0,27)
- hi: Vnitrni vlhkost: nizka, normalni, vysoka (40,80)
- ho: Venkovni vlhkost: nizka, normalni (40)
- co2: Koncentrace CO2: normalni, varovna, vysoka (1000,1500)
- air: Kvalita ovzdusi: normalni, spatna (3) (maximalni hodnota z aqicn)

# Akce

- vyvetrat / zapnout vzduchotechniku
- zapnout topeni
- zapnout klimatizaci

# Priority stavu:

1. (air)
2. co2
3. (to)
4. ti
6. hi
5. (ho)

# TODO

# Jabber
- https://pypi.python.org/pypi/sleekxmpp - listen for messages
- https://pypi.python.org/pypi/jabber-bot/0.1.1 - python2 only
- https://dev.gajim.org/gajim/python-nbxmpp/blob/master/doc/examples/xsend.py

(chtelo by to psat jen lidem co jsou online)

# Telegram

https://pypi.python.org/pypi/telegram-send
```
sudo pip3 install telegram-send
telegram-send --configure --config mvician.conf
```

# Mattermost

https://pypi.python.org/pypi/mattersend/2.0
```
sudo pip3 install mattersend
echo "Hello world!" | mattersend -U https://mattermost.example.com/hooks/XXX
```

# Requirements

sudo apt install python3-gi
sudo pip3 install nbxmpp
